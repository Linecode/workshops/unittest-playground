using System;
using System.Threading.Tasks;

namespace UnitTests.IODependency
{
    public interface IMessage {}

    public interface IMessageBus
    {
        Task Publish(IMessage message);
    }

    public class MessageBus : IMessageBus
    {
        public async Task Publish(IMessage message)
        {
            await Task.Delay(TimeSpan.FromMilliseconds(50));
        }
    }
}