using System;
using System.Threading.Tasks;
using Bogus;

namespace UnitTests.IODependency
{
    public interface IRepository
    {
        Task<Article> Get(Guid id);
        Task Add(Article article);
    }

    public class Repository : IRepository
    {
        public async Task<Article> Get(Guid id)
        {
            await Task.Delay(TimeSpan.FromMilliseconds(30));

            var fakeArticle = new Faker<Article>()
                .RuleFor(x => x.Id, id)
                .RuleFor(x => x.Title, f => f.Lorem.Sentence())
                .RuleFor(x => x.Content, f => f.Lorem.Paragraphs(5));

            return fakeArticle.Generate();
        }

        public async Task Add(Article article)
        {
            await Task.Delay(TimeSpan.FromMilliseconds(30));
        }
    }
}