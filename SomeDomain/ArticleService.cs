using System;
using System.Threading.Tasks;
using UnitTests.IODependency;

namespace UnitTests
{
    public class Article
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
    
    public class ArticleCreated : IMessage {}
    
    public class ArticleService
    {
        private readonly IRepository _repository;
        private readonly IMessageBus _messageBus;

        public ArticleService(IRepository repository, IMessageBus messageBus)
        {
            _repository = repository;
            _messageBus = messageBus;
        }

        public async Task CreateArticle(string title, string content)
        {
            if (string.IsNullOrWhiteSpace(title))
                throw new ArgumentNullException(nameof(title));

            if (string.IsNullOrWhiteSpace(content))
                throw new ArgumentNullException(content);
            
            var article = new Article
            {
                Content = content,
                Title = title
            };

            await _repository.Add(article);
            await _messageBus.Publish(new ArticleCreated());
        }

        public async Task<Article> GetArticle(Guid id)
        {
            return await _repository.Get(id);
        }
    }
}