using System;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using UnitTests;
using Xunit;

namespace Tests
{
    public class ArticleService_Spec
    {
        private readonly IFixture _fixture;
        private readonly ArticleService _sut;

        public ArticleService_Spec()
        {
            _fixture = new Fixture();
            _sut = _fixture.Create<ArticleService>();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public async Task CreateArticle_Should_Check_If_Title_Is_Not_Null(string title)
        {
            await Assert.ThrowsAsync<Exception>(async () =>
            {
                await _sut.CreateArticle(title, _fixture.Create<string>());
            });
        }
    }
}